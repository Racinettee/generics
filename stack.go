package generics

type Stack[X any] struct {
	List[X]
}

func NewStack[X any]() Stack[X] {
	return Stack[X]{}
}

func (stack Stack[X]) Top() X {
	return stack.List[stack.Len()-1]
}
