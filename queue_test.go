package generics

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_NewQueue_And_Push(t *testing.T) {
	queue := NewQueue[int]()

	require.Equal(t, 0, queue.Len())
	queue.Push(1)
	require.Equal(t, 1, queue.Front())
	// pushing a new value doesn't change front
	queue.Push(2)
	require.Equal(t, 1, queue.Front())
	require.Equal(t, 2, queue.Len())
}

func Test_NewQueue_WithElements_AndPop(t *testing.T) {
	queue := NewQueue[int](1, 2, 3, 4)

	require.Equal(t, 1, queue.Front())
	queue.Pop()
	require.Equal(t, 2, queue.Front())
	require.Equal(t, 3, queue.Len())
}
