package generics

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_NewStack(t *testing.T) {
	stack := NewStack[int]()

	require.Equal(t, 0, stack.Len())
}

func Test_Stack_PushPop(t *testing.T) {
	stack := NewStack[int]()

	stack.Push(1)
	stack.Push(2)
	stack.Push(3)

	require.Equal(t, 3, stack.Top())
	require.Equal(t, 3, stack.Len())

	stack.Pop()

	require.Equal(t, 2, stack.Top())
	require.Equal(t, 2, stack.Len())
}

func Test_Stack_Append(t *testing.T) {
	stack := NewStack[int]()

	stack.Append(1, 2, 3, 4)

	require.Equal(t, 4, stack.Len())
	require.Equal(t, 4, stack.Top())
}
