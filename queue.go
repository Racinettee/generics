package generics

type Queue[X any] struct {
	List[X]
}

// inspect the front of the queue
func (queue Queue[X]) Front() X {
	return queue.List[0]
}

// inspect the back fo the queue
func (queue Queue[X]) Back() X {
	return queue.List[queue.Len()-1]
}

// remove the front element of the queue
func (queue *Queue[X]) Pop() X {
	res := queue.Front()
	queue.List = queue.List[1:]
	return res
}

// initialize a new queue
func NewQueue[X any](initialValues ...X) Queue[X] {
	result := Queue[X]{List: make([]X, 0)}
	if len(initialValues) > 0 {
		result.Append(initialValues...)
	}
	return result
}
