package generics

// IsOneOf allows you to compare one value against many and returns true if subject is in values
func IsOneOf[T comparable](subject T, values ...T) bool {
	for _, value := range values {
		if subject == value {
			return true
		}
	}
	return false
}

// IsNoneOf is the same as IsOneOf but returns true only if subject is not in the value set of values
func IsNoneOf[T comparable](subject T, values ...T) bool {
	for _, value := range values {
		if subject == value {
			return false
		}
	}
	return true
}
