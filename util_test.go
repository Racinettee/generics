package generics

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_IsOneOf(t *testing.T) {
	require.True(t, IsOneOf(5, 1, 2, 3, 4, 5))
	require.False(t, IsOneOf(6, 1, 2, 3, 4, 5))
}

func Test_IsNoneOf(t *testing.T) {
	require.True(t, IsNoneOf(6, 1, 2, 3, 4, 5))
	require.False(t, IsNoneOf(1, 1, 2, 3, 4, 5))
}
