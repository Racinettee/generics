package generics

import (
	"github.com/stretchr/testify/require"
	"testing"
)

// Test that a new list is initialized the way you want it
func Test_NewList(t *testing.T) {
	list := NewList[int](0)
	require.Equal(t, 0, list.Len())

	list = NewList[int](10)
	require.Equal(t, 10, list.Len())
}

// Test that push and pop affect the size and append the expect values
func Test_PushPop(t *testing.T) {
	list := NewList[int](0)

	list.Push(1)
	require.Equal(t, 1, list.Len())
	require.Equal(t, 1, list[0])
	list.Push(2)
	require.Equal(t, 2, list.Len())
	require.Equal(t, 2, list[1])

	list.Pop()
	require.Equal(t, 1, list.Len())
	require.Equal(t, 1, list[0])
}

func Test_Append(t *testing.T) {
	list := NewList[int](0)

	list.Append(1, 2, 3, 4)
	require.Equal(t, 4, list.Len())
	require.Equal(t, 1, list[0])
	require.Equal(t, 2, list[1])
	require.Equal(t, 3, list[2])
	require.Equal(t, 4, list[3])

}
